﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    public int health = 1;
    public GameObject PlasmaExplosionEffect;

    public void TakeDamage(int damageAmount)
    {
        health -= damageAmount;

        if (health <= 0)
        {
            Destroy(gameObject);
            Instantiate(PlasmaExplosionEffect, transform.position, transform.rotation);
        }
    }

    void Explode()
    {
        var exp = gameObject.GetComponent<ParticleSystem>();
        exp.Play();
        Destroy(gameObject);
    }
}