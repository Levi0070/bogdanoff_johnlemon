﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    public float turnSpeed = 20f;

    Animator m_Animator;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;

    public Transform hitBox;

    public int count;
    public Text countText;
    public Text exitText;

    public int health = 10;
    public Text healthText;
    bool invincible = false;
    public GameEnding gameEnding;

    bool endExitText;

    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();

        count = 12;
        SetCountText();
        SetHealthText();

        exitText.enabled = false;
    }

    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();

        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        m_Animator.SetBool("IsWalking", isWalking);

        if (isWalking)
        {
            if (!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop();
        }

        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation(desiredForward);
    }

    void OnAnimatorMove()
    {
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);
        m_Rigidbody.MoveRotation(m_Rotation);
    }

    void OnTriggerEnter(Collider other)
    {
       if (other.gameObject.CompareTag ("PickUp"))
        {
            other.gameObject.SetActive(false);
            count = count - 1;
            SetCountText();
            if (count <= 0)
            {
                //gameEnding

            }
        }
    }

    private void Update() 
    {
        SetHealthText();

        if (Input.GetKeyDown(KeyCode.Space))
        {
            
            Collider[] hitColliders = Physics.OverlapBox(hitBox.transform.position, hitBox.transform.localScale / 2, hitBox.transform.rotation);

            for (int i = 0; i < hitColliders.Length; i++)
            {
                if(hitColliders[i].gameObject.CompareTag("Enemy"))
                {
                    EnemyHealth eHealth = hitColliders[i].gameObject.GetComponent<EnemyHealth>();

                    if (eHealth != null)
                        eHealth.TakeDamage(1);
                    
                } 
            } 

        }

        if (count == 0 && !endExitText)
        {
            Invoke("EnableExitText", .1f);
            endExitText = true;
        }
    }

    void SetCountText()
      {
          countText.text = "Things to clean left: " + count.ToString();
          
      }

    void SetHealthText()
    {
        healthText.text = "Health: " + health;
    }


    public void TakeDamage(int damage)
    {
        if (health > 0 && !invincible)
        {
            health -= damage;

            invincible = true;
            Invoke("EndInvincible", 1f);

            if (health <= 0)
            {
                gameEnding.CaughtPlayer();
            }
        }
    }

    void EndInvincible()
    {
        invincible = false;
    }

    void EnableExitText()
    {
        exitText.enabled = true;
        Invoke("DisableExitText", 3f);
    }

    void DisableExitText()
    {
        exitText.enabled = false;
        endExitText = true;
    }

}
