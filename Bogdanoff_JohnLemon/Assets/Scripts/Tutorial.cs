﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour
{
    public Text tutText;
   
    void Start()
    {
        Invoke("DisableText", 5f);
    }

    void DisableText()
    {
        tutText.enabled = false;
    }

}
